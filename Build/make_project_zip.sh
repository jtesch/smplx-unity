#!/bin/bash
pushd ../..

filedate=$(date '+%Y%m%d')
archivename=./SMPLX_UnityProject_$filedate.zip
if [ -f $archivename ]; then
  echo "Removing old file: $archivename"
  rm $archivename
fi

zip -r $archivename SMPLX-Unity/Assets/* SMPLX-Unity/ProjectSettings/* SMPLX-Unity/README.md SMPLX-Unity/LICENSE.md
popd
